#!/usr/bin/env bash

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# ** FUNCTIONS
function randomCowQuote
{
    # Not limited to only cows; names found in /usr/share/cows
    readonly myCows="bud-frogs\ntux\nsmall\ndefault\nmoose\n"
    theCow="$(printf ${myCows} | shuf -n 1)"

    fortune -s | cowsay -f "${theCow}" | lolcat
}

# ** ENV VARS
export TERM="xterm-256color"

# ** DEFAULT APPS
export VISUAL="vim"
export EDITOR="${VISUAL}"
export MANPAGER="sh -c 'col -bx | bat -l man -p'"   # bat as manpager

# ** HISTORY
export HISTCONTROL=ignoredups:erasedups
shopt -s histappend
shopt -s cmdhist
shopt -s lithist

# ** KEYBOARD/COMPLETION
if [[ -r "/usr/share/bash-completion/bash_completion" ]]; then
    . /usr/share/bash-completion/bash_completion
else
    complete -cf sudo
fi

set -o vi
bind -m vi-command "Control-l: clear-screen"
bind -m vi-insert "Control-l: clear-screen"

stty -ixon

bind "set completion-ignore-case on"

shopt -s cdspell
shopt -s dirspell
shopt -s direxpand
shopt -s dotglob
shopt -s nocaseglob

# ** ALIASES
alias vi="vim"

alias rm="rm -Iv"
alias cp="cp -iv"
alias mv="mv -iv"

alias ls="exa --color=always --icons"
alias la="ls -a"
alias ll="ls -l"
alias lx="ls -la"

alias lg="ls --git-ignore"
alias lga="lg -a"
alias lgg="lg -l"
alias lgl="lg -l"
alias lgx="lg -la"

alias tree="exa --color=always --icons -T"
alias t="tree"
alias ta="tree -a"
alias tt="tree -l"
alias tl="tree -l"

alias df="pydf"

alias du="du -h"
alias free="free -h"

alias grep="grep --color=always"

# ** PROMPT
eval "$(starship init bash)"

# ** RANDOM QUOTE
randomCowQuote
