# .dotfiles
VB's dotfiles.
> :warning: **DON'T USE** unless you are VB

## Deploy
```bash
$ paru --needed -S - < pkglist.txt
$ paru --clean
$ systemctl --user --now enable pipewire pipewire-pulse wireplumber
$ rm ~/.bashrc ~/.bash_profile
$ stow .
$ cd dmenu
$ sudo make clean install
$ # Logout for .bash_profile to be sourced
$ startx
```

## Notes
* Wallpapers are set with `nitrogen`, images from the wallpaper packs are in `/usr/share/backgrounds` and `/usr/share/wallpapers`
* Monitor resolution is set with `lxrandr`
* Themes are set with `lxappearance`
* Audio effects (EQ, Bass boost) are automatically started, you can control these using `easyeffects`
