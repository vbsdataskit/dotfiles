#!/usr/bin/env bash
# improved/customized version of https://gist.github.com/olmokramer/b28ff8ed5fd366e3ebb23b79915ec850

hash herbstclient xrandr

function tags
{
    for tag in $(herbstclient tag_status "$1"); do
        name=${tag#?}
        state=${tag%$name}
        case "$state" in
            # focused
            '#')
                printf '%%{B#C678DD}%%{F#4E5561} %s  %%{F}%%{B}' "$name"
                ;;
            # tag is viewed on the specified monitor, but the monitor is not focused
            # '+')
            #     printf '%%{F#cccccc}%%{R} %s %%{R}%%{F-}' "$name"
            #     ;;
            # contains urgent window
            '!')
                printf '%%{R} %s!  %%{R}' "$name"
                ;;
            # empty
            '.')
                printf '%%{F#4E5561} %s  %%{F}' "$name"
                ;;
            *)
                printf ' %s  ' "$name"
        esac
    done
    printf '\n' 
}

geom_regex='[[:digit:]]\+x[[:digit:]]\++[[:digit:]]\++[[:digit:]]\+'
geom=$(xrandr --query | grep "^$MONITOR" | grep -o "$geom_regex")
monitor=$(herbstclient list_monitors | grep "$geom" | cut -d: -f1)

tags "$monitor"

IFS="$(printf '\t')" herbstclient --idle | while read -r hook args; do
	case "$hook" in
	tag*)
		tags "$monitor"
		;;
	esac
done
