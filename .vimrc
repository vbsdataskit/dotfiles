set nocompatible
exec pathogen#infect()

let leader = ","

syntax enable
filetype plugin indent on
colorscheme onedark

let g:rainbow_active = 1
set laststatus=2
set noshowmode
let g:lightline = {
      \ 'colorscheme': 'one',
      \ }
set termguicolors
set backspace=indent,eol,start
set history=400
set ruler
set showcmd
set wildmenu
set wildmode=longest,list,full
set wildignore+=.swp
set nottimeout
set display+=lastline
set scrolloff=1
set sidescrolloff=5
set incsearch
set ignorecase
set hlsearch
set smartcase
set nrformats-=octal
set mouse=
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set smarttab
set autoindent
set smartindent
set cindent
let c_comment_strings=1
set nolangremap
set encoding=UTF-8
set fileformat=unix
set clipboard=unnamedplus
set splitbelow
set splitright
set noea
set linebreak
set wrap
set lazyredraw
set complete-=i
set tabpagemax=64
set number
set noerrorbells
set novisualbell
set background=dark
set title
set foldmethod=indent
set foldnestmax=3
set nofoldenable
set autoread
set autowrite
set confirm
set formatoptions+=j
set nospell
set nobackup
set writebackup
set undofile
set swapfile
set undodir=/tmp
set dir=/tmp
map <F4> :set paste!<CR>
set fillchars+=vert:\ 

" jump to last place in file on opening
autocmd BufReadPost *
            \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
            \ |   exe "normal! g`\""
            \ | endif
