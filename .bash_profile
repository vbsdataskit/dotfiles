#!/usr/bin/env bash

# ** QT theme
export QT_QPA_PLATFORMTHEME=qt5ct

# ** PATH
if [[ -d "${HOME}/Applications" ]]; then
    export PATH="${HOME}/Applications:${PATH}"
fi

if [[ -d "${HOME}/.local/bin" ]]; then
    export PATH="${HOME}/.local/bin:${PATH}"
fi

if [[ -d "${HOME}/bin" ]]; then
    export PATH="${HOME}/bin:${PATH}"
fi

[[ -f ~/.bashrc ]] && . ~/.bashrc
